FROM arm32v7/alpine:edge

ENV SNAP_SERVER=""
ENV AUDIO_CARD=sndrpihifiberry
ENV HOSTID=""

RUN apk add --no-cache snapcast

CMD /usr/bin/snapclient -h ${SNAP_SERVER} -s ${AUDIO_CARD} --hostID ${HOSTID}
